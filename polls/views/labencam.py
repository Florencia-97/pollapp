from django.utils import timezone
import datetime
from ..models import Choice, Question

import os

PERSISTENT = (os.getenv('MEMORY', 'False') == 'False')

class ChoiceMemory():

    def __init__(self, choice_text):
        self.choice_text = choice_text

class Poll():

    def __init__(self, question_text, publishing_date, choices = []):
        self.question_text = question_text
        self.pub_date =  publishing_date
        # choices debería ser un set
        self.choices = list(map(lambda choice_text: ChoiceMemory(choice_text), choices))
        self.results = [0] * len(choices)

    def validate(self):
        if (len(self.choices) < 2):
            raise Exception("There must be at least two choices")
        unique_choices = set(list(map( lambda choice: choice.choice_text, self.choices)))
        if (len(unique_choices) != len(self.choices)):
            raise Exception("Choices can't be repeated")
        if (len(self.question_text.strip()) < 3):
            raise Exception("Question must have at least three letters")

class Adapter():

    def __init__(self, memoryLabencam):
        self.labencam = memoryLabencam

    def create_poll(self, question_text, choices, publishing_date = timezone.now()):
        return self.labencam.create_poll(question_text, choices, publishing_date)

    def get_id_of_poll(self, poll):
        return self.labencam.get_id_of_poll(poll)

    def get_id_of_choice(self, poll, choice):
        return poll.choices.index(choice)

    def published_polls(self):
        return self.labencam.published_polls()

    def poll_for_id(self, question_id, if_not_found_then):
        try :
            polls_archive = self.labencam._polls_archive
            return polls_archive.poll_for_id(question_id)
        except(IndexError):
            if_not_found_then()
    
    def poll_choices(self, poll):
        return poll.choices

    def published_poll_for_id(self, question_id, if_not_found_then):
        poll = self.poll_for_id(question_id, if_not_found_then)
        if (poll.pub_date > timezone.now()):
            if_not_found_then()
        return poll

    def vote(self, poll, choice_id):
        choice = poll.choices[choice_id]
        self.labencam.vote(poll, choice)
    
    def reset(self):
        self.labencam.__init__(PollsArchive())


class PollsArchive:

    def __init__(self):
        self._polls = []
    
    def add_poll(self, poll):
        self._polls.append(poll)
        return poll
    
    def get_id_of_poll(self, poll):
        return self._polls.index(poll)

    def _search_by(self, query):
        return list(filter(lambda poll: query(poll), self._polls))

    def polls_before_date(self, date):
        polls = self._search_by(lambda poll: poll.pub_date <= date)
        polls.sort(key=lambda poll: poll.pub_date, reverse=True)
        return polls
    
    def polls_after_date(self, date):
        return self._search_by(lambda poll: poll.pub_date > date)

    def poll_for_id(self, index):
        return self._polls[index]

    def poll_choice_for_id(self, poll, choice):
        return poll.choices[self.get_id_of_choice(poll, choice)]
    
    def get_id_of_choice(self, poll, choice):
        return poll.choices.index(choice)
    
    def vote_poll_choice_for_id(self, poll, choice):
        choice_id = self.get_id_of_choice(poll, choice)
        poll.results[choice_id] += 1


class Labencam():

    def __init__(self, archive):
        self._polls_archive = archive 

    def create_poll(self, question_text, choices, publishing_date = timezone.now()):
        poll = Poll(question_text, publishing_date, choices)
        poll.validate()
        return self._polls_archive.add_poll(poll)
    
    def get_id_of_poll(self, poll):
        return self._polls_archive.get_id_of_poll(poll)

    def vote(self, poll, choice):
        if (not self._is_published(poll)):
            raise Exception("This poll isn't published yet")
            return
        self._polls_archive.vote_poll_choice_for_id(poll, choice)
        return

    def _is_published(self, poll):
        now = timezone.now()
        publishing_date = poll.pub_date
        return publishing_date <= now
    
    def _is_not_published(self, poll):
        return not self._is_published(poll)

    def published_polls(self):
        return self._polls_archive.polls_before_date(timezone.now())

    def future_polls(self):
        return self._polls_archive.polls_after_date(timezone.now())

class PersistentPollsArchive():

    def add_poll(self, poll):
        question_text = poll.question_text
        pub_date = poll.pub_date
        question = Question.objects.create(question_text=question_text, pub_date=pub_date)
        for choice_text in poll.choices:
            Choice.objects.create(question=question, choice_text=choice_text)
        return question

    def get_id_of_poll(self, poll):
        return poll.id

    def polls_before_date(self, date):
        return Question.objects.filter(pub_date__lte=date).order_by('-pub_date')

    def polls_after_date(self, date):
        return Question.objects.filter(pub_date__lte=date).order_by('-pub_date')

    def poll_for_id(self, question_id):
        return Question.objects.get(pk=question_id)

    def get_id_of_choice(self, choice):
        return choice.id
    
    def poll_choices(self, poll):
        return poll.choice_set.all()
    
    def poll_choice_for_id(self, question, choice_id):
        selected_choice = question.choice_set.get(pk=choice_id)
    
    def vote_poll_choice_for_id(self, question, choice_id):
        selected_choice = self.poll_choice_for_id(question, choice_id)
        selected_choice.votes += 1


class PersistentLabencam():

    def __init__(self, archive):
        self._polls_archive = archive

    def create_poll(self, question_text, choices, time = timezone.now()):
        poll = Poll(question_text, time, choices)
        poll.validate()
        return self._polls_archive.add_poll(poll)
    
    def get_id_of_poll(self, poll):
        return self._polls_archive.get_id_of_poll(poll)

    def vote(self, poll, choice_id):
        if (not self._is_published(poll)):
            raise Exception("This poll isn't published yet")
            return
        selected_choice = self._polls_archive.vote_poll_choice_for_id(poll, choice_id)
        selected_choice.save()
    
    def _is_published(self, poll):
        now = timezone.now()
        publishing_date = poll.pub_date
        return publishing_date <= now
    
    def _is_not_published(self, poll):
        return not self._is_published(poll)
    
    def get_id_of_choice(self, poll, choice):
        return self._polls_archive.get_id_of_choice(choice)

    def published_polls(self): 
        return self._polls_archive.polls_before_date(timezone.now())
    
    def poll_for_id(self, question_id, if_not_found_then):
        try :
            return self._polls_archive.poll_for_id(question_id)
        except(KeyError, Question.DoesNotExist):
            if_not_found_then()

    def published_poll_for_id(self, question_id, if_not_found_then):
        poll = self.poll_for_id(question_id, if_not_found_then)
        if (poll.pub_date > timezone.now()):
            if_not_found_then()
        return poll
    
    def poll_choices(self, poll):
        return self._polls_archive.poll_choices(poll)

    def reset(self):
        return

class Environment():

    _labencam = None

    def __init__(self):
        if (Environment._labencam): return
        if (PERSISTENT):
            #persistentLabencam = Labencam(PersistentPollsArchive())
            #Environment._labencam = Adapter(persistentLabencam)
            Environment._labencam = PersistentLabencam(PersistentPollsArchive())
        else:
            memorylabencam = Labencam(PollsArchive())
            Environment._labencam = Adapter(memorylabencam)

    def labencam(self):
        return Environment._labencam

# Tenemos ahora los dos repositorios
# En memoria -> PollsArchive()
# Peristente -> PersistenPollArchive()

# Por otro lado tenemos dos Labencam, que antes representaban el que sea en memoria o persistente