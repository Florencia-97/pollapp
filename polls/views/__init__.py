from django.http import HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from django.utils import timezone

from ..models import Choice, Question
from .labencam import Environment

def if_not_found_then():
    raise Http404('No polls are available.')

def question_response(question):
    question_id = Environment().labencam().get_id_of_poll(question)
    return {'id': question_id, 'question_text': question.question_text}

def choice_response(question, choice):
    choice_id = Environment().labencam().get_id_of_choice(question, choice)
    return {'id': choice_id, 'choice_text': choice.choice_text}

class IndexView(generic.View):

    def get(self, request):
        template_name = 'polls/index.html'
        questions = Environment().labencam().published_polls()
        latest_question_list = list(map( lambda question: question_response(question), questions))
        context = {'latest_question_list': latest_question_list}
        return render(request, template_name, context)

class DetailView(generic.View):

    def get(self, request, pk):
        template_name = 'polls/detail.html'
        try :
            question = Environment().labencam().published_poll_for_id(pk, if_not_found_then)
        except():
            return render(request, template_name, {'error_message' :"No polls are available."})
        choices = Environment().labencam().poll_choices(question)
        choices_response = list(map( lambda choice: choice_response(question, choice), choices ))
        context = {'question': question_response(question),
                    'choices': choices_response }
        return render(request, template_name, context)

class ShowView(generic.View):

    def get(self, request, pk):
        template_name = 'polls/show.html'
        question = Environment().labencam().poll_for_id(pk, if_not_found_then)
        context = {'question': question}
        return render(request, template_name, context)


class ResultsView(generic.View):
    
    def get(self, request):
        template_name = 'polls/results.html'
        questions = Environment().labencam().published_polls()
        context = {'question_list': questions}
        return render(request, template_name, context)

def vote(request, question_id):
    try:
        question = Environment().labencam().poll_for_id(question_id, if_not_found_then)
        choice_id = request.POST['choice']
        Environment().labencam().vote(question, choice_id)
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'polls/detail.html', {
            'question': question_response(question),
            'error_message': "You didn't select a choice.",
        })
    return HttpResponseRedirect(reverse('polls:show', args=(question_id,)))