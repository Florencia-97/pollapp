from django.urls import path

from . import views

app_name = 'polls'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('results', views.ResultsView.as_view(), name='results'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/show/', views.ShowView.as_view(), name='show'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
]

