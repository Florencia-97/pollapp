import datetime

from django.utils import timezone
from ..views import Environment

def create_question(question_text, days):
    """
    Create a question with the given `question_text` and published the
    given number of `days` offset to now (negative for questions published
    in the past, positive for questions that have yet to be published).
    """
    time = timezone.now() + datetime.timedelta(days=days)
    choices = ['Yes', 'No']
    return Environment().labencam().create_poll(question_text, choices, time)

def question_response(question):
    question_id = Environment().labencam().get_id_of_poll(question)
    text = question.question_text
    return {'id': question_id, 'question_text': text}