import datetime

from django.test import TestCase
from django.utils import timezone
from ..views.labencam import Labencam, PollsArchive

class PollIndexMemoryTests(TestCase):

    def setUp(self):
        self.choices = ['Yes', 'No']
        self.labencam = Labencam(PollsArchive())

    def test_no_polls(self):
        # When
        polls = self.labencam.published_polls()
        # Then
        self.assertEqual(polls, [])

    def test_one_poll(self):
        # Given
        question_text = "How are you?"
        self.labencam.create_poll(question_text, self.choices)
        # When
        polls = self.labencam.published_polls()
        # Then
        self.assertEqual(polls[0].question_text, question_text )
        self.assertEqual(len(polls), 1 )

    def test_many_polls(self):
        # Given
        question_text1 = "How are you?"
        self.labencam.create_poll(question_text1, self.choices)
        question_text2 = "Its sunny?"
        self.labencam.create_poll(question_text2, self.choices)
        # When
        polls = self.labencam.published_polls()
        # Then
        self.assertEqual(len(polls), 2 )
        self.assertEqual(polls[0].question_text, question_text1 )
        self.assertEqual(polls[1].question_text, question_text2 )

    def test_not_published(self):
        # Given
        future_publishing_date = timezone.now() + datetime.timedelta(days=30)
        question_text = "Why?"
        self.labencam.create_poll( question_text, self.choices, future_publishing_date)
        # When
        published_polls = self.labencam.published_polls()
        future_polls = self.labencam.future_polls()
        # Then
        self.assertEqual(published_polls, [])
        self.assertEqual(len(future_polls), 1)
        self.assertEqual(future_polls[0].question_text, question_text)

class ShowPollMemoryTests(TestCase):

    def setUp(self):
        self.choices = ['Yes', 'No']
        self.labencam = Labencam(PollsArchive())

    def test_many_choices(self):
        # Given
        question_text = "How are you?"
        self.labencam.create_poll(question_text, self.choices)
        # When
        poll = self.labencam.published_polls()[0]
        # Then
        for i,choice in enumerate(poll.choices):
            self.assertEqual(choice.choice_text, self.choices[i])
        self.assertEqual(poll.question_text, question_text)

class VotePollMemoryTest(TestCase):

    def setUp(self):
        self.choices = ['Yes', 'No']
        self.labencam = Labencam(PollsArchive())
        self.question_text = "Cold?"
        self.labencam.create_poll(self.question_text, self.choices)
        future_publishing_date = timezone.now() + datetime.timedelta(days=30)
        self.labencam.create_poll(self.question_text, self.choices, future_publishing_date)
    
    def test_vote_new_poll(self):
        # When
        choice_index = 0
        poll = self.labencam.published_polls()[0]
        self.labencam.vote(poll, self.choices[choice_index])
        # Then 
        for i, vote in enumerate(poll.results):
            if (i == choice_index):
                self.assertEqual(poll.results[i], 1)
            else:
                self.assertEqual(poll.results[i], 0)

    def test_vote_new_poll(self):
        # When
        choice_index = 0
        poll = self.labencam.published_polls()[0]
        self.labencam.vote(poll, poll.choices[choice_index])
        # Then 
        for i, vote in enumerate(poll.results):
            if (i == choice_index):
                self.assertEqual(poll.results[i], 1)
            else:
                self.assertEqual(poll.results[i], 0)

    def test_vote_future_poll(self):
        # When
        choice_index = 0
        poll = self.labencam.future_polls()[0]
        # Then
        self.assertRaises(Exception, self.labencam.vote, poll, self.choices[choice_index])

class PollsValidationTest(TestCase):
    
    def setUp(self):
        self.labencam = Labencam(PollsArchive())
    
    def test_poll_without_choices(self):
        # When
        choices = []
        # Then
        self.assertRaises(Exception, self.labencam.create_poll, 'Hello?', choices)
    
    def test_poll_with_one_choice(self):
        # When
        choices = ['No']
        # Then
        self.assertRaises(Exception, self.labencam.create_poll, 'Hello?', choices)
    
    def test_poll_many_different_choices(self):
        # When
        choices = ['No', 'Yes', 'Maybe']
        question_text = 'Like pizza?'
        # Then
        poll = self.labencam.create_poll(question_text, choices)
        self.assertEqual(poll.question_text, question_text)
    
    def test_poll_repeated_choices(self):
        # When
        choices = ['No', 'No', 'Yes']
        # Then
        self.assertRaises(Exception, self.labencam.create_poll, 'Hello?', choices)
    
    def test_question_text_short(self):
        # When
        question_text = 'a'
        # Then
        self.assertRaises(Exception, self.labencam.create_poll, question_text, ['yes', 'no'])
    
    def test_question_short_with_spaces(self):
        # When
        question_text = '  a    '
        # Then
        self.assertRaises(Exception, self.labencam.create_poll, question_text, ['yes', 'no'])
