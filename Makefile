run-in-memory:
	export MEMORY='True' && python3 manage.py runserver

run-persistent:
	export MEMORY='False' && python3 manage.py runserver

run-test-memory:
	export MEMORY='True' && clear && python3 manage.py test polls

run-test-persistent:
	export MEMORY='False' && clear && python3 manage.py test polls

run-test-all:
	make run-test-memory && make run-test-persistent

